export interface MaudArticle {
  id: number,
  title: string,
  date: number,
  long_description: string,
  images: string[]
}
