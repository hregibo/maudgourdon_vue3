# Create a new image to build the production code

FROM node:lts-slim as builder
WORKDIR /app
COPY . .
RUN apt-get update && apt-get install -yyy --assume-yes --no-install-recommends python3 make gcc
RUN npm ci
RUN npm run build

# Build final image for production

FROM nginx:alpine
COPY --from=builder /app/dist /usr/share/nginx/html
COPY --from=builder /app/nginx.conf /etc/nginx/conf.d/default.conf
